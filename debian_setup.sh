sudo curl -fsSLo /usr/share/keyrings/mullvad-keyring.asc https://repository.mullvad.net/deb/mullvad-keyring.asc
echo "deb [signed-by=/usr/share/keyrings/mullvad-keyring.asc arch=$( dpkg --print-architecture )] https://repository.mullvad.net/deb/stable $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/mullvad.list

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt update && sudo apt upgrade
sudo apt install neovim git awesome tmux ripgrep ncdu brightnessctl mullvad-vpn brave-browser
sudo apt install pasystray mate-power-manager

git clone https://gitlab.com/davidnk/dotfiles.git
rm ~/.bashrc
ln -s /home/david/dotfiles/.bashrc /home/david/.bashrc
ln -s /home/david/dotfiles/.inputrc /home/david/.inputrc
ln -s /home/david/dotfiles/.tmux.conf /home/david/.tmux.conf
ln -s /home/david/dotfiles/.config/nvim/ /home/david/.config/nvim
mkdir /home/david/.config/nvim/vim_sessions
ln -s /home/david/dotfiles/.config/awesome/ /home/david/.config/awesome
ln -s /home/david/dotfiles/bin/ /home/david/bin

