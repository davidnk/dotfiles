local vim = vim
vim.cmd('source ~/.config/nvim/vimrc')  -- renamed from init.vim to avoid collision

function SetDefaultOpts()
  -- set ruler
  -- set autoindent
  -- set laststatus=2
  -- set backspace=2
  -- set smarttab
  -- set wildmenu  " tab complete options for command mode
  -- set guicursor=
  -- set history=10000
  -- set incsearch
  -- set hlsearch
  -- set hidden      " Let us move between buffers without writing them
  -- set t_Co=256    " terminal colors if not detecting correctly
  -- set pastetoggle=<F2>
  -- set scrolloff=5               " Min lines above/below the cursor
  vim.opt.number = true
  vim.opt.relativenumber = true
  vim.opt.smartindent = true
  vim.opt.tabstop = 2
  vim.opt.shiftwidth = 2
  vim.opt.expandtab = true  -- expand tabs with spaces
  vim.opt.shiftround = true -- always round indents to multiple of shiftwidth
  vim.opt.mouse = ''
  vim.opt.complete = '.,w,b,u,U,t,i,d'  -- do lots of scanning on tab completion
  vim.opt.undofile = true  -- undos persist after closing file
  vim.opt.undodir = vim.fn.expand('~/.vimundos')
  vim.opt.undolevels = 1000  -- this is defualt
  vim.opt.ignorecase = true    -- search ignoring case
  vim.opt.smartcase = true     -- unless you use uppercase
  vim.opt.foldmethod = 'indent'
  vim.opt.foldnestmax = 6
  vim.opt.foldlevelstart = 20
  vim.opt.wrap = false
  vim.opt.wildignore = '*.o,*.obj,*.bak,*.exe,*.a,*.dep'
  vim.opt.background = 'dark'
  vim.opt.fileformat = 'unix'  -- Set line endings
  vim.opt.directory = '/tmp'   -- Set directory for swp files
  vim.opt.matchpairs:append('<:>')
  vim.opt.clipboard = 'unnamedplus'  -- neovim on x11 requires xclip
  vim.opt.tags = './tags;'
  vim.opt.timeout = true
  vim.opt.timeoutlen = 300  -- 1/3 second between keys
  vim.opt.ttimeoutlen = 100
  vim.opt.cursorline = true  -- highlight current line
  vim.opt.cursorcolumn = true  -- highlight current column
end

function ScratchBuffer()
  vim.cmd('enew')
  vim.bo.buftype = 'nofile'
  vim.bo.bufhidden = 'hide'
  vim.bo.swapfile = false
end

function CloseHiddenBuffers()
  local open_buffers = {}
  for i = 1, vim.fn.tabpagenr('$') do
    vim.list_extend(open_buffers, vim.fn.tabpagebuflist(i))
  end
  for i = 1, vim.fn.bufnr('$') do
    if vim.fn.buflisted(i) == 1 and not vim.tbl_contains(open_buffers, i) then
      vim.cmd('bdelete ' .. i)
    end
  end
end

function GotoDef()
  local actions = {
    function() vim.cmd('normal! gF') end,
    function() vim.cmd('tag ' .. vim.fn.expand('<cword>')) end,
    function() vim.cmd('Cscope find g ' .. vim.fn.expand('<cword>')) end,
    function() vim.cmd('normal! gd') end
  }

  for _, action in ipairs(actions) do
    local success = pcall(action)
    if success then return end
  end
end

local function setup_vim_maps()
  -- goto stuff under cursor
  vim.keymap.set('n', '<Enter>', ':lua GotoDef()<CR>', { silent = true })

  -- Create Scrap/Scratch buffer commands
  vim.api.nvim_create_user_command('Scrap', 'lua ScratchBuffer()', {})
  vim.api.nvim_create_user_command('Scratch', 'lua ScratchBuffer()', {})

  -- Get children
  vim.keymap.set('n', '<Space>gc', '"zyiw:lua ScratchBuffer()<CR>:read !~/.config/nvim/vim_helpers/get_children.py <C-r>z<CR>')
  --cscope_maps.vim for more maps

  -- Search into scrap
  vim.keymap.set('n', 'gr', '"zyiw:lua ScratchBuffer()<CR>:read !rg --line-number ')
  vim.keymap.set('n', 'gr<Enter>', '"zyiw:lua ScratchBuffer()<CR>:read !rg --line-number <C-r>z<CR>')

  -- Grep from list
  vim.keymap.set('n', '<Space>gf', '"zyiw:tab split<CR>:lua ScratchBuffer()<CR>:read !ls /tmp/a<CR>:read !grep_from_list.py /<C-r>z<CR>')
end

local function declare_plugins()
  local Plug = vim.fn['plug#']

  vim.call('plug#begin')

  Plug('easymotion/vim-easymotion')
  Plug('justinmk/vim-sneak')
  Plug('tpope/vim-repeat')
  --Plug('ggandor/leap.nvim')  -- more active, less complete sneak
  --Plug('ggandor/flit.nvim')  -- leap for single chars
  --Plug('folke/flash.nvim') -- extremely configurable

  Plug('mhinz/vim-grepper')
  Plug('tpope/vim-fugitive') -- stuff like :Git blame
  Plug('airblade/vim-gitgutter') --\hs to stage hunk    [c ]c to move between hunks     Breaks :tab split
  Plug('terryma/vim-multiple-cursors') -- C-n to use  -- deprecated
  Plug('brooth/far.vim')
  Plug('tpope/vim-obsession') -- session tracking
  Plug('godlygeek/tabular') -- generalized version of table mode
  -- Plug(paulocesar/neovim-db')  -- run sql queries directly from a vim buffer
  -- Plug('dhruvasagar/vim-table-mode')
  -- Plug('scrooloose/nerdtree')
  -- Plug('tmux-plugins/vim-tmux-focus-events')
  -- Plug('roxma/vim-tmux-clipboard')
  -- Plug('Shougo/denite.nvim') -- better version of unite
  -- Plug('bfredl/nvim-miniyank') -- hack to allow for block copy on osx
  -- Plug('endel/vim-github-colorscheme')
  -- Plug('brettanomyces/nvim-editcommand') -- Plug 'brettanomyces/nvim-terminus'

  Plug('craigemery/vim-autotag')
  Plug('majutsushi/tagbar') -- show skeleton of code based on ctags
  Plug('dhananjaylatkar/cscope_maps.nvim') -- native cscope support was removed so this adds it back

  Plug('fisadev/FixedTaskList.vim') -- show list of todos, fixmes, etc
  Plug('ervandew/supertab') -- tab completion
  Plug('numirias/semshi', {['do'] = ':UpdateRemotePlugins'}) -- python semantic highlighting
  Plug('hynek/vim-python-pep8-indent')
  Plug('davidhalter/jedi-vim')
  Plug('janko-m/vim-test')
  Plug('psf/black')
  vim.g.black_linelength = 120


  -- treesitter does syntax highlighting / language parser stuff (useful if lsp doesn't work)
  -- configured in init.lua
  Plug('nvim-treesitter/nvim-treesitter', {['do'] = ':TSUpdate'})
  Plug('nvim-treesitter/nvim-treesitter-refactor')
  -- Plug('m-demare/hlargs.nvim')

  --  language sserver protocol stuff
  --  configured in init.lua, check installations with :MasonInstall
  Plug('neovim/nvim-lspconfig') -- Configs for the Nvim LSP client (:help lsp) (:help lsp-config)
  Plug('williamboman/mason.nvim', {['do'] = ':MasonUpdate' }) -- installer for lsps
  Plug('williamboman/mason-lspconfig.nvim', {['do'] = ':MasonUpdate' }) -- easy config for Mason installed lsps

  vim.call('plug#end')
end

local function configure_plugins()
  -- [[ Settings for vim-easymotion
    vim.keymap.set('n', '<space>f', '<Plug>(easymotion-s)')
    vim.keymap.set('i', 'jf', '<C-o><Plug>(easymotion-s)')
    vim.g.EasyMotion_smartcase = 1
    vim.api.nvim_create_user_command('E', 'Explore', {})
  --]]

  -- [[ sneak
    vim.keymap.set('', 's', 's')
    vim.keymap.set('', 'S', 'S')
    --vim.g['sneak#label'] = 1  -- allow jumping to any two letter combo
    vim.keymap.set('', 'f', '<Plug>Sneak_f')
    vim.keymap.set('', 'F', '<Plug>Sneak_F')
    vim.keymap.set('', 't', '<Plug>Sneak_t')
    vim.keymap.set('', 'T', '<Plug>Sneak_T')
  --]]

  --[[ flash
    require("flash").toggle()
  --]]

  --[[ leap
    vim.keymap.set({'n', 'x', 'o'}, 's',  '<Plug>(leap)')
    --vim.keymap.set({'n', 'x', 'o'}, 's',  '<Plug>(leap-forward)')
    vim.keymap.set({'n', 'x', 'o'}, 'S',  '<Plug>(leap-backward)')
    vim.keymap.set({'n', 'x', 'o'}, 'gs', '<Plug>(leap-from-window)')
    require('leap.user').set_repeat_keys(';', ',', {
      -- False by default. If set to true, the keys will work like the
      -- native semicolon/comma, i.e., forward/backward is understood in
      -- relation to the last motion.
      --relative_directions = true,
      -- By default, all modes are included.
      --modes = {'n', 'x', 'o'},
    })
  --]]
  --[[ flit
    require('flit').setup {
      keys = { f = 'f', F = 'F', t = 't', T = 'T' },
      -- A string like "nv", "nvo", "o", etc.
      labeled_modes = "", --"vn",
      -- Repeat with the trigger key itself.
      clever_repeat = true,
      multiline = true,
      -- Like `leap`s similar argument (call-specific overrides).
      -- E.g.: opts = { equivalence_classes = {} }
      opts = {}
    }
  --]]

  -- Grepper
    vim.keymap.set('n', '<C-f>', ':Grepper -tool rg<CR>')

  -- Tagbar
    vim.keymap.set('n', '<F8>', ':TagbarToggle<CR>', {silent = true})

  -- vim-python-pep8-indent
    vim.g.pymode_indent = 0

  -- [[ Settings for supertab
    vim.g.SuperTabDefaultCompletionType = '<c-n>'

  -- [[ Settings for jedi-vim
    vim.g['jedi#goto_assignments_command'] = '<leader>g'
    vim.g['jedi#goto_definitions_command'] = '<leader>d'
    vim.g['jedi#documentation_command'] = '<leader>k'
    vim.g['jedi#usages_command'] = '<leader>n'
    -- vim.g['jedi#completions_command'] = '<C-Space>'
    vim.g['jedi#rename_command'] = '<leader>r'
    -- vim.g['jedi#popup_on_dot'] = 0
    -- vim.g['jedi#popup_select_first'] = 0
    vim.g['jedi#use_splits_not_buffers'] = 'left'
    vim.o.completeopt = vim.o.completeopt .. ',longest,menuone'
  --]]

  -- [[ Settings for Obsession
    vim.keymap.set('n', '<Space><Space>q', ':Obsession ~/.config/nvim/vim_sessions/prev_closed.vim<CR>:qa<CR>', {silent = true})
    vim.keymap.set('n', '<Space>s', ':Obsession ~/.config/nvim/vim_sessions/')
    vim.keymap.set('n', '<Space><Space>s', ':Obsession ~/.config/nvim/vim_sessions/')
  --]]

  -- [[ Settings for semshi
    vim.g['semshi#mark_selected_nodes'] = 2
    vim.g['semshi#self_to_attribute'] = vim.NIL
    -- vim.g['semshi#no_default_builtin_highlight'] = vim.NIL
    -- vim.g['semshi#simplify_markup'] = vim.NIL
    vim.g['semshi#excluded_buffers'] = {'*'} -- disable by default
  --]]

  -- [[ vim-test settings
    vim.g['test#strategy'] = 'neovim'
    vim.g['test#python#runner'] = 'pytest'
    vim.keymap.set('n', '<Space>j', ':TestSuite<CR>', {silent = true})
  --]]

  require'nvim-treesitter.configs'.setup {
    -- A list of parser names, or "all" (the five listed parsers should always be installed)
    ensure_installed = { "lua", "vim", "vimdoc", "query" },

    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = false,

    -- Automatically install missing parsers when entering buffer
    -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
    auto_install = false,

    -- List of parsers to ignore installing (for "all")
    -- ignore_install = { "javascript" },

    ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
    -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

    highlight = {
      enable = true,

      -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
      -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
      -- the name of the parser)
      -- list of language that will be disabled
      -- disable = { "c", "rust" },
      -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
      disable = function(lang, buf)
          local max_filesize = 100 * 1024 -- 100 KB
          local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
          if ok and stats and stats.size > max_filesize then
              return true
          end
      end,

      -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
      -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
      -- Using this option may slow down your editor, and you may see some duplicate highlights.
      -- Instead of true it can also be a list of languages
      additional_vim_regex_highlighting = false,
    },
    refactor = {
      highlight_current_scope = { enable = false },
      highlight_definitions = {
        enable = false,
        clear_on_cursor_move = true,  -- Set to false if you have an `updatetime` of ~100.
      },
      navigation = {
        enable = true,
        -- Assign keymaps to false to disable them, e.g. `goto_definition = false`.
        keymaps = {
          goto_definition = "gnd",
          list_definitions = "gnD",
          list_definitions_toc = "gO",
          goto_next_usage = "<a-*>",
          goto_previous_usage = "<a-#>",
        },
      },
    },
  }
  -- require('hlargs').setup()

  require('mason').setup()
  require('mason-lspconfig').setup({
    ensure_installed = {
      -- Replace these with whatever servers you want to install
      --'pylsp',
    }
  })

  local lsp_attach = function(client, bufnr)
    -- Create your keybindings here...
    local bufopts = { noremap=true, silent=true, buffer=bufnr }
    vim.keymap.set("n", 'gD', vim.lsp.buf.declaration, bufopts)
    vim.keymap.set("n", 'gd', vim.lsp.buf.definition, bufopts)
    vim.keymap.set("n", 'gi', vim.lsp.buf.implementation, bufopts)
    vim.keymap.set("n", '<F2>', vim.lsp.buf.rename, bufopts)
    vim.keymap.set("n", '<space>ca', vim.lsp.buf.code_action, bufopts)
    vim.keymap.set(
      "v",
      '<space>ca',
      "<ESC><CMD>lua vim.lsp.buf.range_code_action()<CR>",
      bufopts
    )
  end

  local lspconfig = require('lspconfig')
  require('mason-lspconfig').setup_handlers({
    function(server_name)
      lspconfig[server_name].setup({
        on_attach = lsp_attach,
      })
    end,
  })

  require("cscope_maps").setup({})
end

declare_plugins()
configure_plugins()
setup_vim_maps()
vim.api.nvim_create_user_command('CloseHiddenBuffers', CloseHiddenBuffers, {})
vim.keymap.set('i', '<NUL>', '<C-x><C-o>', { silent = true })
vim.keymap.set('i', '<C-j>', '<C-x><C-o>', { silent = true })
