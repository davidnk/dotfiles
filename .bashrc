# If not running interactively, don't do anything
[ -z "$PS1" ] && return

export VIM_HOME=$HOME/.config/nvim
export BASHRC_PATH=$HOME/.bashrc
export BASHRC_LOCAL_PATH=$HOME/.bashrc_local_stuff
PATH=$HOME/bin:$PATH
PATH=$HOME/dotfiles/bin:$PATH

#. $BASHRC_LOCAL_PATH
alias vim=nvim.appimage

# append to the history file, don't overwrite it
shopt -s histappend

HISTFILESIZE=10000000
HISTSIZE=10000000
HISTCONTROL=ignoreboth
HISTIGNORE='ls:cd:cat:bg:fg:history'
shopt -s cmdhist
PROMPT_COMMAND='history -a'

# set the default editor
EDITOR=vim; export EDITOR

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
shopt -s globstar

alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# colored vs uncolored terminal prompt
#PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '

alias ls='ls --color=auto -X'

# psql scrolling results open in nvim
# export DB_PAGER='nvim.appimage -c ":setlocal buftype=nofile" -c ":setlocal bufhidden=hide" -c ":setlocal noswapfile" -'
# alias psql="PAGER="'"$DB_PAGER"'" psql"

alias svim='sudoedit'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias ack='ack-grep -i'
ack-vim() {
  (vack.py "$@") | vim -c ":setlocal buftype=nofile" -c ":setlocal bufhidden=hide" -c ":setlocal noswapfile" -
  echo
}
alias vack=ack-vim
alias g=ack-vim
find-vim() {
  find . | grep -i "$@" | vim -c /"$@" -c ":setlocal buftype=nofile" -c ":setlocal bufhidden=hide" -c ":setlocal noswapfile" -
  echo
}
alias vfind=find-vim
alias f=find-vim

alias tl='tmux list-sessions'
alias ta='tmux attach -t "$@"'
alias tad='tmux attach -dt "$@"'
_autocomplete_tmux_attach() {
    local cur opts
    cur="${COMP_WORDS[COMP_CWORD]}"
    opts=$(tmux ls | awk '{print $1}' | sed 's/://g' | xargs)
    COMPREPLY=($(compgen -W "${opts}" -- ${cur}))
}
complete -F _autocomplete_tmux_attach ta

vim-session() {
  if [ -z "$1" ]; then
    ls -1 $VIM_HOME/vim_sessions/
  else
    vim -S $VIM_HOME/vim_sessions/$1*
  fi
}
_autocomplete_vim_session() {
    local cur opts
    cur="${COMP_WORDS[COMP_CWORD]}"
    opts=$(ls -1 $VIM_HOME/vim_sessions/ | awk '{print $1}' | sed 's/://g' | xargs)
    COMPREPLY=($(compgen -W "${opts}" -- ${cur}))
}
complete -F _autocomplete_vim_session vim-session
alias v=vim-session
complete -F _autocomplete_vim_session v

alias index='cscope_gen.sh && ctags -R .'

[ -s "$HOME/.scm_breeze/scm_breeze.sh" ] && source "$HOME/.scm_breeze/scm_breeze.sh"
alias gds='gd --staged'

# most recent branch checkout by Diego
#recent_git_branches() {
#    git reflog | egrep -io "moving from ([^[:space:]]+)" | awk '{ print $3 }' | awk ' !x[$0]++' | head -n10 | nl
#    read  -n 1 -p "Checkout branch:" line_num
#    echo " "
#    if [ $line_num = "q" ]; then
#     return 1
#    fi  
#    branch=$(git reflog | egrep -io "moving from ([^[:space:]]+)" | awk '{ print $3 }' | awk ' !x[$0]++' | head -n6 | sed -n "$line_num p")
#    echo "checking out $branch"
#    git checkout $branch
#}
#alias gr=recent_git_branches```


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

export PS1='\w$ '

if [ -f $BASHRC_LOCAL_PATH ]; then
    . $BASHRC_LOCAL_PATH

    # Load mtime at bash start-up
    export BASHRC_LOCAL_MTIME=$(git hash-object $BASHRC_LOCAL_PATH)
    PROMPT_COMMAND="check_and_reload_bashrc_local; $PROMPT_COMMAND"
    check_and_reload_bashrc_local () {
      # if [ "$(git hash-object $BASHRC_LOCAL_PATH)" != $BASHRC_LOCAL_MTIME ]; then
      if [[ "$(git hash-object $BASHRC_LOCAL_PATH)" != $BASHRC_LOCAL_MTIME ]]; then
        export BASHRC_LOCAL_MTIME="$(git hash-object $BASHRC_LOCAL_PATH)"
        echo "bashrc_local_stuff changed. re-sourcing..." >&2
        . $BASHRC_PATH
      fi
    }
fi

# Load mtime at bash start-up
#echo "bashrc mtime: $(git hash-object $HOME/.bashrc)" >&2
export BASHRC_MTIME=$(git hash-object $BASHRC_PATH)

PROMPT_COMMAND="check_and_reload_bashrc; $PROMPT_COMMAND"
check_and_reload_bashrc () {
  # if [ "$(git hash-object $BASHRC_PATH)" != $BASHRC_MTIME ]; then
  if [[ "$(git hash-object $BASHRC_PATH)" != $BASHRC_MTIME ]]; then
    export BASHRC_MTIME="$(git hash-object $BASHRC_PATH)"
    echo "bashrc changed. re-sourcing..." >&2
    . $BASHRC_PATH
  fi
}
